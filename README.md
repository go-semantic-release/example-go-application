# example-go-application

This example repository shows how to use go-semantic-release to release a Go application with GitLab CI.

Releases are created automatically when a commit is pushed to the main branch. All releases can be found on the [releases page](https://gitlab.com/go-semantic-release/example-go-application/-/releases).
